#ifndef __DTGEN_BINDINGS_H__
#define __DTGEN_BINDINGS_H__

#include <yaml-cpp/yaml.h>
#include <string>
#include <list>
#include "fdt.h"
#include "binding.h"

namespace dtgen
{
  class Bindings
  {
    public:
      void parse(std::istream& yaml);
      bool process(const FDT::Node& node, BindingOutput& output);

    private:
      const Binding& match(const FDT::Node& node);
      std::map<std::string, Binding> bindings;
  };
}

#endif // __DTGEN_BINDINGS_H__

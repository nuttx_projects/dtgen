#include <iostream>
#include <regex>
#include "bindings.h"

using namespace std::string_literals;

void dtgen::Bindings::parse(std::istream& yaml)
{
  YAML::Node yaml_node = YAML::Load(yaml);
  std::string compatible = yaml_node["compatible"].as<std::string>();

  std::cerr << "Loading binding for compatible: " << compatible << std::endl;
  bindings.emplace(compatible, yaml_node);
}

bool dtgen::Bindings::process(const FDT::Node& node, BindingOutput& output)
{
  /* try to match by compatible string */

  std::list<std::string> compatible = node.property<std::list<std::string>>("compatible");

  for (const std::string& item : compatible)
  {
    //std::cerr << "Looking compatible binding for: " << item << std::endl;

    auto it = bindings.find(item);
    if (it != bindings.end() && it->second.matches(node))
    {
      /* match found, generate output according to binding */

      if (it->second.matches_children())
      {
        std::cout << "Matched " << node.name() << " children with compatible: " << item << std::endl;

        for (FDT::Node child = node.child(); child; child = child.next())
        {
          it->second.generate(child, output);
        }
      }
      else
      {
        std::cout << "Matched " << node.name() << " with compatible: " << item << std::endl;
        it->second.generate(node, output);
      }

      return true;
    }
  }

  return false;
}
